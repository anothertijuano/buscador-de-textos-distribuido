# Buscador de textos distribuido

![Java](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white, "Java")
![Javascript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E, "Javascript")



Sistema distribuido buscador de palabras en textos.

Desarrollado para la materia de Desarrollo de sistemas distribuidos impartida por el profesor Ukranio Coronilla Contreras en la Escuela Superior de Cómputo del IPN.

## Arquitectura

```mermaid
flowchart TD
    Buscador("Buscador")
    client("Cliente HTML") --->|Palabras a buscar| Buscador

    Buscador -->|Lista ordenada de resultados| client 
    subgraph ide0 [Systema de busqueda]
        direction TB
        Buscador -->|Titulos, Palabras| ide1
        ide1 -->|Titulos, Valores| Buscador
        subgraph ide1 [Nodos de busqueda]
            ndb0(Nodo de busqueda 0)
            ndb0 --- db0[(Textos)]
            ndb(...)
            ndb --- db[(Textos)]
            ndbn(Nodo de busqueda n)
            ndbn --- dbn[(Textos)]
        end
    end
```
El cliente HTML está diseñado para ser desplegado de manera independiente al resto del sistema. La comunicación entre cliente y buscador se lleva a cabo mediante la API fetch para peticiones HTTP.

## Diseño

## Algoritmo IDF-TF

En el algoritmo **IDF-TF** se le asigna un *valor* a cada texto en el que se realiza la busqueda a partir del valor de cada palabra *P*.

``` math
\begin{aligned}

&valor = \sum_{i=1}^{len(P)} {IDF(i)}*{TF(i)} \\

\\
\text{Donde:}\\
\\
&TF(i) = {{f(i)} \over {len(T)} }\\
\\
&IDF(i) = log_{10}({{NT}\over{Q(i)}})\\
\\
&P: \text{Arreglo de palabras a buscar}\\
\\
&T: \text{Arreglo de palabras en el que se realiza la busqueda}\\
\\
&f(i): \text{Número de apariciones del i-esimo elemento de P en T}\\
\\
&NT: \text{Número de textos}\\
\\
&Q(i): \text{Número de textos que contienen el i-esimo elemento de P}\\
\\
\end{aligned}

```






## Instalación




## Uso

Los módulos del sistema se comunican mediante HTTP. 




## Autor

David Andrade Olvera



## Status del proyecto

En desarrollo, no funcional.


---

© 2022 IPN.
